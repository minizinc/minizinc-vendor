version=$(git ls-remote --tags https://github.com/ERGO-Code/HiGHS | fgrep tags/v | cut -d '/' -f 3 | sort -nr -t. -k1,1 -k2,2 -k3,3 | head -1)
echo "export HIGHS_VERSION=\"${version}\""
