version=$(git ls-remote --tags https://github.com/google/or-tools | grep -o 'tags/v[0-9]*\..*' | cut -c 7- | sort -nr -t. -k1,1 -k2,2 | head -1)
echo "export OR_TOOLS_VERSION=\"v${version}\""
