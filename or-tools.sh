#!/bin/bash
set -e
set -x

# Download OR-Tools source
source or-tools-version.sh
git clone --depth 1 --branch ${OR_TOOLS_VERSION} https://github.com/google/or-tools ${CI_PROJECT_DIR}/or-tools

mkdir -p {build,vendor}/or-tools
cd build/or-tools

# Calculate patch version from GitHub
if [[ -z "$OR_TOOLS_MAJOR_VERSION" ]]; then
  OR_TOOLS_MAJOR_VERSION="v$(echo "$OR_TOOLS_VERSION" | cut -c 2- | cut -d. -f1).0"
fi
export OR_TOOLS_PATCH=$(curl -Ls "https://api.github.com/repos/google/or-tools/compare/${OR_TOOLS_MAJOR_VERSION}...${OR_TOOLS_VERSION}" | sed -n 's/.*"total_commits"[[:space:]]*:[[:space:]]*\([0-9][0-9]*\).*/\1/p')

cmake -G"$CMAKEARCH" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$CI_PROJECT_DIR/vendor/or-tools" "$CI_PROJECT_DIR/or-tools" -DCMAKE_OSX_ARCHITECTURES="x86_64;arm64" \
      -DBUILD_DEPS=ON -DUSE_COINOR=OFF -DUSE_HIGHS=OFF -DUSE_SCIP=OFF -DBUILD_EXAMPLES=OFF -DBUILD_SAMPLES=OFF -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=OFF
cmake --build . --config Release
cmake --build . --config Release --target install
