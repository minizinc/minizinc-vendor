#!/bin/bash
set -e
set -x

source cbc-version.sh

# Download coinbrew script
curl -L https://raw.githubusercontent.com/coin-or/coinbrew/master/coinbrew -o coinbrew
chmod u+x coinbrew

# Base configure args
config_opts="--verbosity=4 \
--parallel-jobs=2 \
--enable-static --disable-shared \
--without-blas --without-lapack --without-mumps --disable-bzlib \
--no-third-party \
--skip-update \
--tests none"


if [[ "$MZNARCH" == "linux" ]]; then
	config_opts+=" --enable-cbc-parallel"
elif [[ "$MZNARCH" == "linux-arm64" ]]; then
	config_opts+=" --enable-cbc-parallel --build=aarch64-unknown-linux-gnu"
elif [[ "$MZNARCH" == "osx" ]]; then
	config_opts+=" --enable-cbc-parallel"
elif [[ "$MZNARCH" == "wasm" ]]; then
	# Use emconfigure and emmake
	sed -i 's/"$config_script"/emconfigure "$config_script"/g; s/$MAKE/emmake $MAKE/g' coinbrew
	config_opts+=" CXXFLAGS=-std=c++14"
elif [[ "$MZNARCH" == "win64" ]]; then
	config_opts+=" --enable-msvc --build=x86_64-w64-mingw32"
	export CI_PROJECT_DIR=`cygpath ${CI_PROJECT_DIR}`
else
	echo "Illegal MZNARCH value"
	exit 1
fi

config_opts+=" --prefix=${CI_PROJECT_DIR}/vendor/cbc"

# Fetch and build CBC
./coinbrew --no-prompt fetch --no-third-party Cbc@${CBC_VERSION}
./coinbrew --no-prompt build Cbc ${config_opts}
