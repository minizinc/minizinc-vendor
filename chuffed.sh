#!/bin/bash
set -e
set -x

mkdir -p {build,vendor}/chuffed
cd build/chuffed

cmake -G"$CMAKEARCH" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/vendor/chuffed $CI_PROJECT_DIR/chuffed -DCMAKE_OSX_ARCHITECTURES="x86_64;arm64"
cmake --build . --config Release
cmake --build . --config Release --target install
