version=$(git ls-remote --tags https://github.com/coin-or/Cbc | fgrep releases | cut -d '/' -f 4 | sort -nr -t. -k1,1 -k2,2 -k3,3 | head -1)
echo "export CBC_VERSION=\"${version}\""
