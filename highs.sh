#!/bin/bash
set -e
set -x

# Download HIGHS source
source highs-version.sh
git clone --depth 1 --branch ${HIGHS_VERSION} https://github.com/ERGO-Code/HiGHS ${CI_PROJECT_DIR}/highs

mkdir -p {build,vendor}/highs
cd build/highs

cmake -G"$CMAKEARCH" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$CI_PROJECT_DIR/vendor/highs" "$CI_PROJECT_DIR/highs" -DCMAKE_OSX_ARCHITECTURES="x86_64;arm64" -DBUILD_SHARED_LIBS=ON -DFAST_BUILD=ON
cmake --build . --config Release
cmake --build . --config Release --target install
