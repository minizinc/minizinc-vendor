#!/bin/bash
set -e
set -x

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <URL> <BRANCH> <DIR>"
    exit 1
fi

changed=1

if [ -d "${3}/.git" ]; then
  cd $3
  git fetch --quiet

  UPSTREAM='@{u}'
  LOCAL=$(git rev-parse @)
  REMOTE=$(git rev-parse "$UPSTREAM")
  BRANCH=$(git rev-parse --abbrev-ref HEAD)

  if [[ $BRANCH = $2 && $LOCAL = $REMOTE ]]; then
    changed=0
  else
    git checkout --force --quiet $2
    git pull --quiet
  fi
else
  rm -rf $3
  mkdir -p $(dirname $3)
  git clone --quiet $1 $3
  cd $3
  git checkout --quiet $2
fi

echo $changed
