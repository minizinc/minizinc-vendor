#!/bin/bash
# Usage: gecode.sh {build_with_gist:0/1}
set -e
set -x

DIR="gecode"
ARCH="x86_64;arm64"
if [ $1 = 1 ]; then
	ENABLE_GIST=TRUE
	if [ ! -x "$(command -v qmake)" ]; then
		  echo "!!!!!!!!!!!!!! CANNOT FIND QMAKE !!!!!!!!!!!!"
		  exit 1
	fi
	DIR="gecode_gist"
else
	ENABLE_GIST=FALSE
fi

mkdir -p {build,vendor}/$DIR
cd build/$DIR

cmake -G"$CMAKEARCH" $CI_PROJECT_DIR/gecode -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/vendor/$DIR -DENABLE_GIST=${ENABLE_GIST} -DENABLE_CPPROFILER=TRUE -DCMAKE_OSX_ARCHITECTURES=${ARCH}
cmake --build . --config Release
cmake --build . --config Release --target install
